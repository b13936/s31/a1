const express = require("express");
const router = express.Router();

const taskController = require("./../controllers/taskControllers");

router.post("/", (req,res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

router.get("/get-task", (req,res) => {
	taskController.getTask(req.body).then(result => res.send(result));
})

router.get("/get-alltask", (req,res) => {
	taskController.getAllTask(req.body).then(result => res.send(result));
})


router.get("/:id", (req, res) => {
	
	taskController.getById(req.params.id).then( result => res.send(result))
})

router.put("/:id/complete", (req, res) => {

	taskController.updateTask(req.params.id).then( result => res.send(result))
})

module.exports = router;