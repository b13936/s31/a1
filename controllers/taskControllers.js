
const Task = require("./../models/Task");


module.exports.createTask = (reqBody) => {

	let newTask = new Task ({
		name: reqBody.name
	})

	return newTask.save(reqBody).then( result => result)
}




// get all tasks

module.exports.getAllTask = (reqBody) => {

	return Task.find({}).then(result => result);
}

module.exports.getById = (params) => {

	return Task.findById(params)
}


module.exports.updateTask = (params) => {

	let updatedStatus = {
		status: "complete"
	}

	return Task.findOneAndUpdate({params}, updatedStatus,{new: "complete"}).then(result => {
		return result
	})
} 